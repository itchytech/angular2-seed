import * as gulp from 'gulp';
import * as gulpLoadPlugins from 'gulp-load-plugins';
import * as merge from 'merge-stream';
import {join} from 'path';
import {APP_SRC,TMP_DIR} from '../../config';
const plugins = <any>gulpLoadPlugins();



export = () => merge(processSCSS(), replaceTSwithCSS()
 );


function processSCSS() {
    let sassOptions = {
        includePaths: ['src/assets']
    };

    return gulp.src([
            join(APP_SRC, '**', '*.scss'),
            '!' + join(APP_SRC, 'assets', '**', '*.scss')
        ])
        .pipe(plugins.sass(sassOptions).on('error', plugins.sass.logError))

        .pipe(gulp.dest(TMP_DIR));
}

function replaceTSwithCSS() {
    return gulp.src([
            join(APP_SRC, '**','*.ts'),
            '!' + join(APP_SRC, '**', '*.e2e.ts'),
            '!' + join(APP_SRC, '**', '*.spec.ts')

        ])
        .pipe(plugins.replace('scss', 'css'))
        .pipe(gulp.dest(TMP_DIR));
}

