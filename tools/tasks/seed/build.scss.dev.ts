import * as gulp from 'gulp';
import * as gulpLoadPlugins from 'gulp-load-plugins';
import * as merge from 'merge-stream';
import {join} from 'path';
import {APP_SRC,APP_DEST} from '../../config';
const plugins = <any>gulpLoadPlugins();

export = () => merge(processSCSS(), processJS()
 );

function processSCSS() {
    let sassOptions = {
        includePaths: ['src/assets']
    };
    let src = [
        join(APP_SRC, '**/*.scss')
    ];

    return gulp.src(src)
        .pipe(plugins.sass(sassOptions).on('error', plugins.sass.logError))
        .pipe(gulp.dest(APP_DEST));

}

function processJS() {
    return gulp.src(join(APP_DEST, '**', '*.js'))
        .pipe(plugins.replace('scss','css'))
        .pipe(gulp.dest(APP_DEST));
}
