import {Component} from 'angular2/core';

@Component({
  selector: 'sd-contact',
  moduleId: module.id,
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {}
